# Sixi MoveIt

MoveIt experimentations with Sixi 2 model and Gazebo simulations

Project name according to ROS : **sixi-moveit**

ROS version : Noetic

### Contribute

1. Install [ROS Noetic](http://wiki.ros.org/noetic/Installation/Ubuntu) on your Ubuntu 20.04 system (VM, dual-boot, WSL)
2. Install [MoveIt](https://ros-planning.github.io/moveit_tutorials/doc/getting_started/getting_started.html) for Noetic
3. Clone this repository in the `src` folder of your catkin workspace
4. Run `catkin build` in your catkin workspace to build the MoveIt ROS package for Sixi 2 robotic arm
5. Contribute! The files that you will edit are probably the YAML configuration files in the `config` folder and the application launch files in the `launch` folder. Redo step 4 whenever you want to test new changes to your code.

### Tips

To launch RViz to visualize the Sixi 2 : https://ros-planning.github.io/moveit_tutorials/doc/quickstart_in_rviz/quickstart_in_rviz_tutorial.html.

To launch the demonstration Gazebo simulation, run `roslaunch sixi-moveit demo_gazebo.launch` after doing steps 1 to 4 in the "Contribute" section above and play with the robotic arm.

To learn how to create a custom MoveIt package for any robotic arm, this section of the MoveIt tutorials is really helpful : https://ros-planning.github.io/moveit_tutorials/#integration-with-a-new-robot.

For information on this repository, contact Katherine Zamudio, robotic arm team lead at PolyOrbite : katherine.zamudio-turcotte@polymtl.ca
